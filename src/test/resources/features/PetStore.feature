Feature: Pruebas sobre la aplicacion gratuita PetStore

  Scenario: Añadir una nueva mascota a la tienda virtual
    When Yo creo la mascota con las siguientes caracteristicas
      | campo        | valor     |
      | id           | 456000    |
      | categoryId   | 456001    |
      | categoryName | UVA       |
      | name         | Doggie    |
      | photoUrls    | prueba    |
      | tagId        | 455999    |
      | tagName      | prueba    |
      | status       | available |
    Then El codigo de respuesta es "200"

  Scenario: Comprobar que hemos añadido la mascota correctamente
    When Yo consulto los datos de la mascota con id "456000"
    Then El codigo de respuesta es "200"
    And El nombre de la mascota corresponde con "Doggie"

  Scenario: Actualizar alguna carácterística de la mascota
    When Yo consulto los datos de la mascota con id "456000"
    Then El codigo de respuesta es "200"
    And Yo modifico el parametro "name" a "Rodolfo" de la mascota con id "456000"
    And El codigo de respuesta es "200"

  Scenario: Compruebo el nuevo parámetro de la mascota
    When Yo consulto los datos de la mascota con id "456000"
    Then El codigo de respuesta es "200"
    And Yo consulto el parametro "name" y tiene que ser "Rodolfo"

  Scenario: Borrar la mascota
    When Yo borro los datos de la mascota con id "456000"
    Then El codigo de respuesta es "200"

  Scenario: Comprobar que la mascota no se está disponible
    When Yo consulto los datos de la mascota con id "456000"
    Then El codigo de respuesta es "404"

