package gherkinSteps;



import cucumber.api.DataTable;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import net.thucydides.core.annotations.Step;
import object.Category;
import object.Pet;
import object.Tags;
import org.junit.Assert;
import utilities.API;
import utilities.World;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PetstoreSteps {

    private World world = World.getWorld();

    @Step
    public void crearMascota(DataTable datatable) {
        System.out.println(datatable);
        API api = new API("https://petstore.swagger.io/v2", "/pet", ContentType.JSON);
        Pet pet = new Pet();
        Category category = new Category();
        Tags tags = new Tags();
        Map<String, Object> body = datatable.asMap(String.class, Object.class);
        pet.setId(Long.parseLong((String) body.get("id")));
        category.setId(Integer.parseInt((String) body.get("categoryId")));
        category.setName((String) body.get("categoryName"));
        pet.setName((String) body.get("name"));
        List<String> items = Arrays.asList(((String) body.get("photoUrls")).split("\\s*,\\s*"));
        pet.setPhotoUrls(items);
        pet.setCategory(category);
        tags.setId(Integer.parseInt((String) body.get("tagId")));
        tags.setName((String) body.get("tagName"));
        pet.setStatus((String) body.get("status"));
        api.setBody(pet);
        api.submitRequest(Method.POST);
        world.setResponse(api.getResponse());
        world.setPet(pet);

    }

    public void comprobarCodigoRespuesta(int codigo) {
        int codigoRespuesta = world.getResponse().getStatusCode();
        Assert.assertEquals("El código de respuesta: " + codigoRespuesta + " no es el esperado: " + codigo,
                codigoRespuesta,
                codigo);
    }

    public void consultarID(long id) {
        API api = new API("https://petstore.swagger.io/v2", "/pet/" + id, ContentType.JSON);
        api.submitRequest(Method.GET);
        world.setResponse(api.getResponse());
        world.setPet(world.getResponse().jsonPath().getObject("", Pet.class));
        api.getResponse().prettyPrint();
    }

    public void comprobarNombre(String nombre) {
        String nombreMascota = world.getPet().getName();
        Assert.assertEquals("El nombre recibido: " + nombreMascota + " no es el que pediamos: " + nombre,
                nombreMascota,
                nombre);
    }

    public void borroLosDatosDeLaMascotaConId(long id) {
        API api = new API("https://petstore.swagger.io/v2", "/pet/" + id, ContentType.JSON);
        api.submitRequest(Method.DELETE);
        world.setResponse(api.getResponse());
        api.getResponse().prettyPrint();
    }

    public void modificoElParametroADeLaMascotaConId(String argumento, Object parametro, long id) {
        Pet pet = world.getPet();
        switch (argumento) {
            case "id":
                pet.setId((long) parametro);
                break;
            case "cateogry.id":
                pet.getCategory().setId((long) parametro);
                break;
            case "cateogry.name":
                pet.getCategory().setName((String) parametro);
                break;
            case "name":
                pet.setName((String) parametro);
                break;
            case "photoUrls":
                pet.setPhotoUrls(Arrays.asList(((String) parametro).split("\\s*,\\s*")));
                break;
            case "tags.id":
                pet.getTags().get(0).setId((long) parametro);
                break;
            case "tags.name":
                pet.getTags().get(0).setName((String) parametro);
                break;
            case "status":
                pet.setStatus((String) parametro);
                break;
            default:
                Assert.fail("Selecciona un parámetro correcto");
                break;
        }
        API api = new API("https://petstore.swagger.io/v2", "/pet", ContentType.JSON);
        api.setBody(pet);
        api.submitRequest(Method.PUT);
        world.setResponse(api.getResponse());
        world.setPet(pet);
    }


    public void consultoElParametroYTieneQueSer(String argumento, Object parametro) {

        Pet pet = world.getPet();
        switch (argumento) {
            case "id":
                Assert.assertEquals("El id "+parametro.toString()+" no es el que tiene la base de datos: "+ pet.getId(),
                        (long)parametro,pet.getId());
                break;
            case "cateogry.id":
                Assert.assertEquals("El id "+parametro.toString()+" no es el que tiene la base de datos: "+ pet.getCategory().getId(),
                        (long)parametro,pet.getCategory().getId());
                break;
            case "cateogry.name":
                pet.getCategory().setName((String) parametro);
                break;
            case "name":
                pet.setName((String) parametro);
                break;
            case "photoUrls":
                pet.setPhotoUrls(Arrays.asList(((String) parametro).split("\\s*,\\s*")));
                break;
            case "tags.id":
                pet.getTags().get(0).setId((long) parametro);
                break;
            case "tags.name":
                pet.getTags().get(0).setName((String) parametro);
                break;
            case "status":
                pet.setStatus((String) parametro);
                break;
            default:
                Assert.fail("Selecciona un parámetro correcto");
                break;
        }
    }
}

