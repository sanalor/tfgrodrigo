package serenityRunner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * Test Runner necesario para ejecutar los tests
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
// Carpeta donde se encuentran las definicion de los test
  features = {"src/test/resources/features"},
  plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"},
  glue = {"gherkinDefinitions"}
)

public class TestRunner {
}
