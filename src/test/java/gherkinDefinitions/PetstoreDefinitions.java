package gherkinDefinitions;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkinSteps.PetstoreSteps;
import cucumber.api.DataTable;
import net.thucydides.core.annotations.Steps;


public class PetstoreDefinitions {
    @Steps
    private PetstoreSteps petstoreSteps;

    @When("^Yo creo la mascota con las siguientes caracteristicas$")
    public void yoCreoLaMascotaConLasSiguientesCaracteristicas(DataTable datatable) {
        petstoreSteps.crearMascota(datatable);
    }

    @Then("^El codigo de respuesta es \"([^\"]*)\"$")
    public void elCodigoDeRespuestaEs(int codigo)  {
        petstoreSteps.comprobarCodigoRespuesta(codigo);
    }

    @When("^Yo consulto los datos de la mascota con id \"([^\"]*)\"$")
    public void yoConsultoLosDatosDeLaMascotaConId(long id) {
        petstoreSteps.consultarID(id);
    }

    @And("^El nombre de la mascota corresponde con \"([^\"]*)\"$")
    public void elNombreDeLaMascotaCorrespondeCon(String name)  {
        petstoreSteps.comprobarNombre(name);
    }

    @When("^Yo borro los datos de la mascota con id \"([^\"]*)\"$")
    public void yoBorroLosDatosDeLaMascotaConId(long id) {
       petstoreSteps.borroLosDatosDeLaMascotaConId(id);
    }

    @And("^Yo modifico el parametro \"([^\"]*)\" a \"([^\"]*)\" de la mascota con id \"([^\"]*)\"$")
    public void yoModificoElParametroADeLaMascotaConId(String argumento, Object parametro, long id) {
        petstoreSteps.modificoElParametroADeLaMascotaConId(argumento, parametro, id);
    }

    @And("^Yo consulto el parametro \"([^\"]*)\" y tiene que ser \"([^\"]*)\"$")
    public void yoConsultoElParametroYTieneQueSer(String argumento, Object parametro) {
        petstoreSteps.consultoElParametroYTieneQueSer(argumento, parametro);
    }
}
