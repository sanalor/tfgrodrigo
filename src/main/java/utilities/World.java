package utilities;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import object.Pet;

import java.util.Properties;

public class World {

    private Pet pet;
    private Response response;


    private static final String SESSION_VARIABLE_NAME = "World";


    public static World getWorld() {
        if (Serenity.hasASessionVariableCalled(SESSION_VARIABLE_NAME)) {
            return Serenity.sessionVariableCalled(SESSION_VARIABLE_NAME);
        } else {
            World world = new World();
            Serenity.setSessionVariable(SESSION_VARIABLE_NAME).to(world);
            return world;
        }
    }

    public static World clearWorld() {
        World world = new World();
        Serenity.setSessionVariable(SESSION_VARIABLE_NAME).to(world);
        return world;
    }


    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}