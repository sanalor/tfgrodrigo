package utilities;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.config.EncoderConfig.encoderConfig;
import static net.serenitybdd.rest.SerenityRest.rest;

public class API {

    private static final int CORRECT_REQUEST = 200;
    private static final int INVALID_ID_SUPPLIED = 400;
    private static final int PET_NOT_FOUND = 404;
    private static final int VALIDATION_EXCEPTION = 405;
    //    private static final int CONFLICT = 409;
//    private static final int INTERNAL_SERVER_ERROR = 500;
    private String domain;
    private String endPoint;
    private Response response;
    private RequestSpecification spec;

    /**
     * Fija los parámetros de cabecera
     *
     * @param key
     * @param value
     */
    public void setHeader(String key, String value) {
        spec.header(key, value);
    }

    /**
     * Fija los parámetros del cuerpo de la solicitud
     *
     * @param body
     */
    public void setBody(Object body) {
        spec.body(body);
    }


    /**
     * API Constructor
     *
     * @param domain      the domain of the restapi
     * @param endPoint    the endpoint of the restapi
     * @param contentType the contenttype
     */
    public API(String domain, String endPoint, ContentType contentType) {
        this.domain = domain;
        this.endPoint = endPoint;
        spec = rest().contentType(contentType).accept(contentType).config(RestAssured.config().encoderConfig(encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)));
    }



    /**
     * @return Devuelve la respuesta de la solicitud
     */
    public Response getResponse() {
        return response;
    }

    /**
     * Executes a request depending on the method received by parameter
     *
     * @param method One of the next values: get, post, put or delete
     */
    public void submitRequest(Method method) {
        String fullPath = domain + endPoint;
        fire(method, fullPath);
    }

    /**
     * Ejecuta la solicitud mediante un determinado método.
     * Por defecto se utiliza el método GET
     *
     * @param method
     * @param fullPath
     */
    private void fire(Method method, String fullPath) {
        switch (method.toString()) {
            case "GET":
                response = spec.get(fullPath);
                break;
            case "POST":
                response = spec.post(fullPath);
                break;
            case "DELETE":
                response = spec.delete(fullPath);
                break;
            case "PUT":
                response = spec.put(fullPath);
                break;
            default:
                response = spec.get(fullPath);
                break;
        }


    }

    /**
     * Devuelve el codigo de respuesta de la solicitud
     *
     * @return
     */
    public int getStatusCode() {
        return response.getStatusCode();
    }
}
