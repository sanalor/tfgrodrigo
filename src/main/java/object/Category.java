package object;

//Clase que define la categoría de una mascota

public class Category {
    private long id; //Definicion del ID de la categoría
    private String name; //Definición del nombre de la categoría

    //Getter y setter del ID
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    //Getter y setter del nombre
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
